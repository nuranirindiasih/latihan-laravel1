<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index');

// Route::get('/register', 'AuthController@registrasi');

// //Route::get('/welcome', 'AuthController@sapa');
// Route::get('/welcome', 'AuthController@submit');

// Route::get('/master', function(){
// 	return view ('adminlte.master');
// });

// Route::get('/items', function(){
// 	return view ('items.index');
// });

// Route::get('/', function(){
// 	return view ('items.tables');
// });

// Route::get('/data-tables', function(){
// 	return view ('items.datatables');
// });

Route::get('/', function(){
	return view ('adminlte.master');
});

// Route::get('/pertanyaan/create', 'PertanyaanController@create');
// Route::post('/pertanyaan', 'PertanyaanController@store');
// Route::get('/pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
// Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
// Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
// Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
// Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');


Route::resource('pertanyaan','PertanyaanController');
