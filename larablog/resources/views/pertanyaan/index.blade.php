@extends('adminlte.master')

@section('content')
	<div class="mt-3 ml-3">
		<div class="card">
      <div class="card-header">
          <h3 class="card-title">Tabel Pertanyaan</h3>
      </div>
        <!-- /.card-header -->
      <div class="card-body">
      	@if(session('success'))
      		<div class="alert alert-success">
      			{{ session('success') }}
      		</div>
      	@endif
      	<a class="btn btn-primary mb-3" href="{{route('pertanyaan.create')}}">Buat Pertanyaan Baru</a>
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Judul</th>
                <th>Isi Pertanyaan</th>
                <th style="width: 40px">Action</th>
              </tr>
          </thead>
            <tbody>
              @forelse($pertanyaan as $key => $post)
              <tr>
              	<td> {{ $key+1 }} </td>
              	<td> {{ $post -> judul }} </td>
              	<td> {{ $post -> isi }} </td>
              	<td style="display: flex;"> 
              		<a class="btn btn-primary btn-sm" href="{{route('pertanyaan.show',['pertanyaan'=> $post->id])}}">tampilkan</a>
              		<a class="btn btn-default btn-sm ml-2" href="/pertanyaan/{{$post->id}}/edit">ubah</a>
              		<form action="/pertanyaan/{{$post->id}}" method="post">
              			@csrf
              			@method('DELETE')
              			<input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
              		</form>
              	</td>
              </tr>
              @empty
              	<tr>
              		<td colspan="4" align="center">Tidak Ada Pertanyaan</td>
              	</tr>
              @endforelse
              
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
        </div> -->
      </div>		

	</div>
@endsection