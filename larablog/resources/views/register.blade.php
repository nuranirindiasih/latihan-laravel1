<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
</head>
<body>

	<div>
		<h1>Buat Account Baru!</h1>	
		<h3>Sign Up Form</h3>		
	</div>

	<form action="/welcome" method="get">
		@csrf
		<label >First name:</label>	<br><br>
		<input type="text" name="namaDepan">			<br><br>
		<label>Last name:</label>	<br><br>
		<input type="text" name="namaBelakang">			<br><br>
	
		<label>Gender:</label>		<br><br>
			<input type="radio" name="gender" value="0"> Male	<br>
			<input type="radio" name="gender" value="1"> Female	<br>
			<input type="radio" name="gender" value="2"> Other	<br><br>
	
		<label>Nationality:</label>	<br><br>
			<select>
				<option value="id">Indonesian</option>
				<option value="sg">Singaporean</option>
				<option value="my">Malaysian</option>
				<option value="au">Australian</option>	
			</select>				<br><br>

		<label>Language Spoken:</label>	<br><br>
			<input type="checkbox" value="language">Bahasa Indonesia 	<br>
			<input type="checkbox" value="language">English				<br>
			<input type="checkbox" value="language">Other				<br><br>

		<label>Bio</label>			<br><br>
			<textarea cols="30" rows="9"></textarea>	<br>

		<input type="submit" value="Sign Up">

	</form>

</body>
</html>