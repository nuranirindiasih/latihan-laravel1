<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create(){
    	return view ('pertanyaan.create');
    }


    public function store(Request $request){
    	//dd($request->all());

    	$request->validate([
    		"judul" => 'required|unique:pertanyaan',
    		"isi" => 'required|unique:pertanyaan'
    	]);

    	//--------Menggunakan Query Builder
        // $query = DB::table('pertanyaan')->insert([
    	// 	"judul" => $request["judul"],
    	// 	"isi" => $request["isi"]
    		
    	// ]);

        //--------Menggunakan Model Eloquent
        $create = Pertanyaan::create([
            "judul" =>  $request["judul"],
            "isi" =>  $request["isi"]
        ]);



    	return redirect('/pertanyaan')->with('success','Pertanyaan Anda Berhasil Disimpan');
    	
    }

    public function index(){
    	//--------Menggunakan Query Builder
        //$pertanyaan = DB::table('pertanyaan')->get();//select*from pertanyaan
    	//dd($pertanyaan);

        //--------Menggunakan Model Eloquent
        $pertanyaan = Pertanyaan::all();


    	return view('pertanyaan.index', compact('pertanyaan'));

    }

    public function show($id){
    	//--------Menggunakan Query Builder
        //$tampil = DB::table('pertanyaan')->where('id',$id)->first();
    	//dd($tampil);

        //--------Menggunakan Model Eloquent
        $tampil = Pertanyaan::find($id);


    	return view('pertanyaan.show', compact('tampil'));
    }

    public function edit($id){
    	//--------Menggunakan Query Builder
        //$ubah = DB::table('pertanyaan')->where('id',$id)->first();


        //--------Menggunakan Model Eloquent
        $ubah = Pertanyaan::find($id);


    	return view('pertanyaan.edit', compact('ubah'));
    }

    public function update($id, Request $request){
    	// $request->validate([
    	// 	"judul" => 'required|unique:pertanyaan',
    	// 	"isi" => 'required|unique:pertanyaan'
    	// ]);

        //--------Menggunakan Query Builder
    	// $query = DB::table('pertanyaan')
    	// 		->where('id',$id)
    	// 		->update([
    	// 			'judul' => $request['judul'],
    	// 			'isi' => $request['isi']
    	// 		]);

        //--------Menggunakan Model Eloquent 
        $update = Pertanyaan::where('id',$id)->update([
            "judul" => $request ["judul"],
            "isi" => $request ["isi"]
        ]);



    	return redirect ('/pertanyaan')->with('success','Berhasil Ubah Pertanyaan');
    }

    public function destroy($id){
        //--------Menggunakan Query Builder
    	//$query = DB::table('pertanyaan')->where('id', $id)->delete();

        //--------Menggunakan Model Eloquent
        Pertanyaan::destroy($id);


    	return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus');
    }
}
